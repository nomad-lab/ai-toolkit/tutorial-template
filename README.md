# analytics-tutorial-template

This repository contains a skeleton of a tutorial with all of the templates. 

To create/integrate a new tutorial there are a few requirements that has to be fulfilled.
Each tutorial has to have a to be installable. 

File structure af a tutorial:
- `metainfo.json`: contains some data about the tutorial. This file is used to update the information on the website. The same data can be used for the python packeage as well. 
- `setup.py`: provides an easy way to access any code available in the python environments. Most importantly it should contain all of the dependencies of the tutorial 
- `data`: folder that stores all the data required in this folder.
- `assets`: folder for storing any additional materials (logos, figure, etc.) 


## Tips and tricks

- Please note that during the deployment these folders (`data`, `assets`) from other tutorials will be merged so if you want to be on the safe side you can use sub-folders as well.
- the namo of th jupyter notebook could be the same as the name of repository. 

## Notes

- More information about the python `setup.py` file: https://docs.python.org/3/distutils/setupscript.html
- `json` vs. `yaml`: Yaml looks nicer but currently only the json format is supported natively by Python.